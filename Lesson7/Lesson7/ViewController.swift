//
//  ViewController.swift
//  Lesson7
//
//  Created by Yauheni Skiruk on 25.01.21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label1: UILabel!
    
    @IBOutlet weak var label2: UILabel!
    
    @IBOutlet weak var label3: UILabel!
    
    @IBOutlet weak var label4: UILabel!
    
    @IBOutlet weak var buttonOutlet: UIButton!
    
    @IBAction func buttonAction(_ sender: Any) {
        print("Button pressed!")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
    }
    private func setupUI(){
        label1.text = "new text"
        label2.backgroundColor = .blue
        label3.backgroundColor = .red
        label3.layer.cornerRadius = 10
        
    
        
    }

}

